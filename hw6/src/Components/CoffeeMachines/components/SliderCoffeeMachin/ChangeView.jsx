import { useContext } from "react"
import { CoffeMachineContext } from "../../../../context"
import './ButtonBlock.scss'






const ChangeView = () => {
    const{display,changeDisplayToTable, changeDisplayToList } = useContext(CoffeMachineContext)
    const tableDisplay = !display


    return (
        <div className='changeDisplay'>
            <button className={display? "btn-options active-btn": "btn-options" } onClick={changeDisplayToList}> Change to list</button>
            <button className={display? "btn-options": "btn-options active-btn" }onClick={changeDisplayToTable}> Change to Table </button>
        </div>
    )

}

export default ChangeView