import SliderCoffeeMachine from "./CoffeeMachine"; 
import { fireEvent, render, waitFor } from "@testing-library/react";
import { Provider, useSelector } from 'react-redux';
import store from "../../../../store"; 
import { selectorModalAddtoBasket } from "../../../../selectors";



describe("CoffeeMachineList", ()=>{
   
    test ('Check List', ()=>{
        const {container} = render(<Provider store={store}><SliderCoffeeMachine/></Provider>)
        const div = container.querySelector('div')
        waitFor(() => expect(div).toHaveClass('coffee-machines__items'))
    })



    test('Button click', ()=>{
        const {container, getByTestId} = render(<Provider store={store}><SliderCoffeeMachine/></Provider>)
        const button = container.querySelector('.coffee-machines__items--button')
        const modal =  /* useSelector(selectorModalAddtoBasket) */ false
        const modallWraper = document.querySelector('.active')
        
        

         waitFor(() => fireEvent.click(button))

        
         expect(modal).toBe(false) 
         expect(modallWraper).toBeInTheDocument()
    })
})