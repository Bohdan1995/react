/* import { useState, useEffect } from "react"; */
import { Component } from 'react';
/* import {Swiper, SwiperSlide} from 'swiper/react' */
import { sendRequest } from "../../../../Helpers/getData";
import PropTypes from 'prop-types';
import "./CoffeeMatchines.scss"


class SliderCoffeeMachine extends Component {
    state = {
        coffeeMachines: [],

       
    }


    componentDidMount() {
        sendRequest('./data.json')
            .then((result) => {
                this.setState(prevState => ({
                    ...prevState,
                    coffeeMachines: result
                }))
            })

    }


  

 








    render() {

        const { coffeeMachines} = this.state
        const { handlerFavorite, handlerBasket, openModal, favorites } = this.props

        const favoriteId = favorites.map(({id})=>{
            return id
        })
    

      



        return (
            <>
                {coffeeMachines.map(({ name, img, article, color, id, price }) => (
                    <div key={id} id={id} className='coffee-machines__items'>
                        <img src={img} alt="photo" />
                        <p className='coffee-machines__items--header'>{name}
                        <span className='favorite-icon' 
                            >  
                        <svg height="23" width="23" fill='grey' className={favoriteId.includes(id) ? "star2" : "star"} onClick={()=>{
                            handlerFavorite({ name, img, article, color, id, price });
                            
                            }} >
                                <polygon points="9.9, 1.1, 3.3, 21.78, 19.8, 8.58, 0, 8.58, 16.5, 21.78" />
                            </svg>
                             </span>
                        </p>
                        <p className='coffee-machines__items--color'> Color: {color}</p>
                        <p className='coffee-machines__items--article'>Article: {article}</p>
                        <p className='coffee-machines__items--price'>Prise: {price} ₴</p>
                       
                          <button className='coffee-machines__items--button' onClick={()=>{
                             openModal()
                             handlerBasket({ name, img, article, color, id, price });        
                        }}>Add to basket</button>
                
                    </div>
                ))}
            </>
        )
    }
}

SliderCoffeeMachine.propTypes = {
    handlerFavorite: PropTypes.func,
    handlerBasket: PropTypes.func,
	openModal: PropTypes.func,
}
 

export default SliderCoffeeMachine









/* const SliderCoffeeMachine = ({handlerCurentFilm, handlerOpenModal}) =>{
    const [coffeeMachine, setCoffeeMachine] = useState([]);

    useEffect(()=>{
        sendRequest
    })
} */