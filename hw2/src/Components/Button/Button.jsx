import { Component } from "react";
import PropTypes from 'prop-types'; 
import "./Buton.scss"

class Button extends Component{
    render(){
        const{backgroundColor, text, openModalReg, clas, addToBasket} = this.props

        return(
                <button className = {clas} style={{background: backgroundColor}}  onClick={()=>{openModalReg(); addToBasket()}}>{text}</button>
        )
    }

}

Button.propTypes = {
    clas: PropTypes.any, 
    openModalReg: PropTypes.func,
    addToBasket: PropTypes.func,
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
}

export default Button