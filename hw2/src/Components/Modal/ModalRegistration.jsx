import {Component} from 'react'
import PropTypes from 'prop-types'
/* import { useEffect, useRef, createRef } from 'react' */

import "./RegistrationModal.scss"

 class Modal extends Component{

    render(){
        const{XBtn, closeModal, headerRegModal, textRegModal, actions} = this.props
        return(
            <div className="modal-wrapper" onClick={closeModal}>
				<div className="modal" onClick={e=>e.stopPropagation()}>
					<div className="modal-box">
						{XBtn && <button type="button" className="modal-close" onClick={closeModal}>
							<svg viewBox="0 0 16 16" width="16" height="16">
								<path
									d="m9.3 8 6.1-6.1c.4-.4.4-.9 0-1.3s-.9-.4-1.3 0L8 6.7 1.9.6C1.6.3 1 .3.6.6c-.3.4-.3 1 0 1.3L6.7 8 .6 14.1c-.4.4-.3.9 0 1.3l.1.1c.4.3.9.2 1.2-.1L8 9.3l6.1 6.1c.4.4.9.4 1.3 0s.4-.9 0-1.3L9.3 8z"/>
							</svg>
						</button>}
						<div className="modal-header">
							<h4>{headerRegModal}</h4>
						</div>
						<div className="modal-content">
							<p>{textRegModal}</p>
						</div>
						<div className="modal-footer">
							{actions}
						</div>
					</div>
				</div>
			</div>
        )
    }
}

Modal.propTypes = {
    XBtn: PropTypes.bool,
    closeModal: PropTypes.func,
	headerRegModal: PropTypes.string,
	textRegModal: PropTypes.string,
	actions: PropTypes.element
}
 

/* function Modal({XBtn, closeModal, headerRegModal, textRegModal, actions}){
	const wrapperRef = useRef();

	useEffect(()=>{
		document.addEventListener('mousedown', handleClickOutside)

		return(()=>{
			document.removeEventListener('mousedown', handleClickOutside)
		})
	})

	const handleClickOutside = (event) =>{
		if(wrapperRef && ! wrapperRef.current.contains(event.target)){
			closeModal();
		}
	}


	return(
		<div className="modal-wrapper">
			<div className="modal" ref={wrapperRef}>
				<div className="modal-box">
					{XBtn && <button type="button" className="modal-close" onClick={closeModal}>
						<svg viewBox="0 0 16 16" width="16" height="16">
							<path
								d="m9.3 8 6.1-6.1c.4-.4.4-.9 0-1.3s-.9-.4-1.3 0L8 6.7 1.9.6C1.6.3 1 .3.6.6c-.3.4-.3 1 0 1.3L6.7 8 .6 14.1c-.4.4-.3.9 0 1.3l.1.1c.4.3.9.2 1.2-.1L8 9.3l6.1 6.1c.4.4.9.4 1.3 0s.4-.9 0-1.3L9.3 8z"/>
						</svg>
					</button>}
					<div className="modal-header">
						<h4>{headerRegModal}</h4>
					</div>
					<div className="modal-content">
						<p>{textRegModal}</p>
					</div>
					<div className="modal-footer">
						{actions}
					</div>
				</div>
			</div>
		</div>
	)
}
 */


export default Modal