/* import { useState, useEffect } from 'react'; */
import {Component} from 'react'
import { Button } from "./Components/Button";
import { Modal } from "./Components/Modal";
import { sendRequest } from './Helpers/getData';
import Header from './Components/Header/Header';
import Footer from './Components/Footer/Footer';
import { Products } from './Components/CoffeeMachines';
import {SliderCoffeeMachine} from './Components/CoffeeMachines/components/SliderCoffeeMachin'




 class App extends Component {
  
  

  state = {
    
    modal: false,
   /*  currentMachineFavorite: {}, */
    favorit: JSON.parse(localStorage.getItem('favorites')) || [],  
    basket: JSON.parse(localStorage.getItem('basket')) || [],  
    currentMachineBasket:{},
    changeColorStarOnklick: false
   
    
  }

  openModal = () => {
    console.log('aaa')
    this.setState((prevState) => {
      return {
        ...prevState,
        modal: !prevState.modal
      }
    })
  }



  handlerCurrentMachines = (currentMachineBasket)=>{
  this.setState((prevState)=>{
    console.log(prevState.currentMachineBasket)
    return{
      ...prevState,
      currentMachineBasket: {...currentMachineBasket}
    }
  })
}
 
handlerBasket = ( currentMachineBasket)=>{
  this.setState((prevState)=>{
    let aaa = true
    
    prevState.basket.forEach((el)=>{
      if(currentMachineBasket.id === el.id){ 
    aaa = false
      }
    }) 
    if(aaa){
      localStorage.setItem('basket', JSON.stringify([...prevState.basket, currentMachineBasket]));
      return{
        ...prevState,
        basket:/*  [...prevState.basket, currentMachineBasket]  */JSON.parse(localStorage.getItem('basket'))
      }
    }
    if(!aaa){
      /* let bbb = JSON.parse(localStorage.getItem('basket'))
      bbb.forEach((el)=> {
        if(el.id===currentMachineBasket.id){
          bbb.splice(bbb.indexOf(el), 1)
        }})
        localStorage.setItem('basket', JSON.stringify(bbb)); */
      return{
        ...prevState,
        basket: /* [...prevState.basket]  */ JSON.parse(localStorage.getItem('basket'))
      }
    }
    
  })
} 
 


handlerFavorite = (favorit)=>{
  this.setState((prevState)=>{
  let aaa = true
    prevState.favorit.forEach((el)=>{
      if(favorit.id === el.id){
        /* prevState.favorit.splice(prevState.favorit.indexOf(el), 1) */
    aaa = false
      }
    }) 
    if(aaa){
      localStorage.setItem('favorites', JSON.stringify([...prevState.favorit, favorit]));
      return{
        ...prevState,
        favorit: /* [...prevState.favorit, favorit] */ JSON.parse(localStorage.getItem('favorites')),
        iconStarColor: true
      }
    
    }
    if(!aaa){
      let bbb = JSON.parse(localStorage.getItem('favorites'))
      bbb.forEach((el)=> {
        if(el.id===favorit.id){
          bbb.splice(bbb.indexOf(el), 1)
        }})

      localStorage.setItem('favorites', JSON.stringify(bbb));
      return{
        ...prevState,
        favorit: /* [...prevState.favorit] */JSON.parse(localStorage.getItem('favorites')),
        iconStarColor: false
      }
    }

  })

} 



changeColor = () => {
  console.log('xxxx')
  this.setState((prevState) => {
      
    return {
      ...prevState,
      changeColorStarOnklick: !prevState.changeColorStarOnklick
    }
  })
}





  render() {
    const { modal, favorit, currentMachineBasket, basket} = this.state
    
    return (
      <div>
        <Header countBasket={basket.length} countFavorit = {favorit.length}/>
        <Products>
          <SliderCoffeeMachine  handlerFavorite={this.handlerFavorite} 
                                openModal={this.openModal}
                                handlerBasket={this.handlerCurrentMachines}
                                changeColor = {this.changeColor}
                                favorites = {favorit} >
         
       
          
            </SliderCoffeeMachine>
        </Products>

        
        {modal && <Modal XBtn={true} closeModal={this.openModal} headerRegModal={"Hello"} textRegModal={'Do you want to add this item to your basket'} actions={<div className="button-wrapper">
          <Button clas={"btn"} text={'ok'} addToBasket={()=>this.handlerBasket(currentMachineBasket)} openModalReg={this.openModal} />
          <Button clas={"btn"} text={'cancel'} openModalReg={this.openModal} />
        </div>} />}
        
        <Footer/>

        



      </div>

    )

  }
}
 

/* function App(){
  const [modal, setModal] = useState(false)

	const handlerToggleModal = () => {
		setModal(!modal);
	}

    return (
      <div>
        <Header/>
        <Button clas={"button"} backgroundColor={"blue"} openModalReg={handlerToggleModal} text={'Open first modal'} />
       
        {modal && <Modal XBtn={true} closeModal={handlerToggleModal} headerRegModal={"Registration form"} textRegModal={'Please enter yor full name'} actions={<div className="button-wrapper">
          <Button clas={"btn"} text={'ok'} openModalReg={handlerToggleModal} />
          <Button clas={"btn"} text={'cancel'} openModalReg={handlerToggleModal} />
        </div>} />}

      

        <Footer/>
      </div>

    )
} */





export default App;
