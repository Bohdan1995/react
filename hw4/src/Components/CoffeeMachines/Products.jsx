import { useState, useEffect } from "react"; 
import './Products.scss'

const Products = ({children})=>{
    return(
        <div className='coffee-machines'>
            {children}
        </div>
    )
} 

export default Products