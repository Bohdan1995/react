import { Component } from "react";
import "./Buton.scss"

class Button extends Component{
    render(){
        const{backgroundColor, text, openModalReg, clas, mainButton} = this.props

        return(
                <button className = {clas} style={{background: backgroundColor}} onClick={openModalReg}>{text}</button>
        )
    }

}

export default Button