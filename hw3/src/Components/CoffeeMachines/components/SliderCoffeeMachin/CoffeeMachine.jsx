import { useState, useEffect } from "react";
import { sendRequest } from "../../../../Helpers/getData";
import PropTypes from 'prop-types';

import "./CoffeeMatchines.scss"

const SliderCoffeeMachine = ({ handlerFavorite, handlerBasket, openModal, favorites }) => {
    const [coffeeMachines, setCoffeeMachines] = useState([]);
   


    useEffect(() => {
        sendRequest('./data.json')
            .then((results) => {
                setCoffeeMachines(results);
            })

    }, [])

    const changeColor = (e) => {
        return !e
    }

    const favoriteId = favorites.map(({id})=>{
        return id
    })

    const item = coffeeMachines.length && coffeeMachines.map(({ name, img, article, color, id, price}) => (
        <div key={id} id={id} className='coffee-machines__items'>
            <div className="biger-size">
                <img src={img} alt="photo" />
            </div>
            <p className='coffee-machines__items--header'>{name}
                <span className='favorite-icon'>
                    <svg height="23" width="23" className={favoriteId.includes(id) ? "star2" : "star"} onClick={() => {
                       
                        handlerFavorite({ name, img, article, color, id, price });
                      



                    }} >
                        <polygon points="9.9, 1.1, 3.3, 21.78, 19.8, 8.58, 0, 8.58, 16.5, 21.78" />
                    </svg>
                </span>
            </p>
            <p className='coffee-machines__items--color'> Color: {color}</p>
            <p className='coffee-machines__items--article'>Article: {article}</p>
            <p className='coffee-machines__items--price'>Prise: {price} ₴</p>

            <button className='coffee-machines__items--button' onClick={() => {
                openModal()
                handlerBasket({ name, img, article, color, id, price });
            }}>Add to basket</button>

        </div>
    ))

    return (
        <>
            {item}

        </>
    )
}



SliderCoffeeMachine.propTypes = {
    handlerFavorite: PropTypes.func,
    handlerBasket: PropTypes.func,
    openModal: PropTypes.func,
}


export default SliderCoffeeMachine









