import { useState } from 'react';
import { Route, Routes } from 'react-router-dom';

import { Button } from "./Components/Button";
import { Modal } from "./Components/Modal";
import Header from './Components/Header/Header';
import Footer from './Components/Footer/Footer';


import { Basket } from './pages/basket'
import { Favorites } from './pages/favotites';
import { MainPage } from './pages/main';




const App = () => {


  const [modal, setModal] = useState(false);
  const [modalDeleteItem, setModalDeleteItem] = useState(false);
  const [favorit, setFavorit] = useState(JSON.parse(localStorage.getItem('favorites')) || []);
  const [basket, setBasket] = useState(JSON.parse(localStorage.getItem('basket')) || [])
  const [currentMachineBasket, setCurrentMachineBasket] = useState({})




  const openModalDeleteItem = () => {
    setModalDeleteItem(!modalDeleteItem)
  }



  const openModal = () => {
    setModal(!modal)
  }

  const handlerCurrentMachines = (currentMachineBasket) => {
    setCurrentMachineBasket({ ...currentMachineBasket })
  }


  const handlerBasket = (currentMachineBasket) => {
    let aaa = true
    basket.forEach((el) => {
      if (currentMachineBasket.id === el.id) {
        aaa = false
      }
    })
    if (aaa) {
      localStorage.setItem('basket', JSON.stringify([...basket, currentMachineBasket]));
      setBasket(JSON.parse(localStorage.getItem('basket')))
    }
    if (!aaa) {
      setBasket(JSON.parse(localStorage.getItem('basket')))
    }

  }



  const handlerFavorite = (currentMachineBasket) => {
    let aaa = true
    favorit.forEach((el) => {
      if (currentMachineBasket.id === el.id) {
        aaa = false
      }
    })
    if (aaa) {
      localStorage.setItem('favorites', JSON.stringify([...favorit, currentMachineBasket]));
      setFavorit(JSON.parse(localStorage.getItem('favorites')))

    }
    if (!aaa) {
      let bbb = JSON.parse(localStorage.getItem('favorites'))
      bbb.forEach((el) => {
        if (el.id === currentMachineBasket.id) {
          bbb.splice(bbb.indexOf(el), 1)
        }
      })
      localStorage.setItem('favorites', JSON.stringify(bbb));
      setFavorit(JSON.parse(localStorage.getItem('favorites')))
    }
  }

  const deleteFromBasket = (currentMachineBasket) =>{
    let bbb = JSON.parse(localStorage.getItem('basket'))
    bbb.forEach((el) => {
      if (el.id === currentMachineBasket.id) {
        bbb.splice(bbb.indexOf(el), 1)
      }
    })
    localStorage.setItem('basket', JSON.stringify(bbb));
    setBasket(JSON.parse(localStorage.getItem('basket')))
  }










  return (
    <div>
      <Header countBasket={basket.length} countFavorit={favorit.length} />
      <Routes>
        <Route path='/' element={< MainPage handlerFavorite={handlerFavorite}
                                             openModal={openModal}
                                             handlerBasket={handlerCurrentMachines}
                                             favorites={favorit} />} />

        <Route path='basket' element={<Basket openModalDeleteItem={openModalDeleteItem}
                                               handlerBasket={handlerCurrentMachines}
                                               basket = {basket}/>}/>


        <Route path='favorites' element={<Favorites  handlerFavorite={handlerFavorite}
                                                      favorites = {favorit}/>}/>
      </Routes>




      {modal && <Modal XBtn={true} closeModal={openModal} headerRegModal={"Hello"} textRegModal={'Do you want to add this item to your basket'} actions={<div className="button-wrapper">
        <Button clas={"btn"} text={'ok'} addToBasket={() => handlerBasket(currentMachineBasket)} openModalReg={openModal} />
        <Button clas={"btn"} text={'cancel'} openModalReg={openModal} />
      </div>} />}

      {modalDeleteItem && <Modal XBtn={true} closeModal={openModalDeleteItem} headerRegModal={"Hello"} textRegModal={'Do you want to delete this item from your basket'} actions={<div className="button-wrapper">
        <Button clas={"btn"} text={'ok'} addToBasket={() => deleteFromBasket(currentMachineBasket)} openModalReg={openModalDeleteItem} />
        <Button clas={"btn"} text={'cancel'} openModalReg={openModalDeleteItem} />
      </div>} />}


      <Footer />
    </div>

  )
}

export default App;
